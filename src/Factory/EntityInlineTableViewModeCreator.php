<?php

namespace Drupal\ief_table_view_mode\Factory;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\ief_table_view_mode\Form\EntityInlineTableViewModeForm;

class EntityInlineTableViewModeCreator {

  /**
   * @var EntityFieldManagerInterface
   */
  private $entityFieldManager;
  /**
   * @var EntityTypeManagerInterface
   */
  private $entityTypeManager;
  /**
   * @var ModuleHandlerInterface
   */
  private $moduleHandler;

  public function __construct (
    EntityFieldManagerInterface $entityFieldManager,
    EntityTypeManagerInterface $entityTypeManager,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->moduleHandler = $moduleHandler;
  }

  public function createForEntityType (string $entityType) : EntityInlineTableViewModeForm {
    return new EntityInlineTableViewModeForm(
      $this->entityFieldManager,
      $this->entityTypeManager,
      $this->moduleHandler,
      $this->entityTypeManager->getDefinition($entityType)
    );
  }
}
